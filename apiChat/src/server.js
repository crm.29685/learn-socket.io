const express = require("express");
const http = require('http');
const { Server } = require("socket.io");
const cors = require('cors');

const routerAuth = require('./router/routerAuth');
const routerRooms = require('./router/routerRooms');
const { SocketAddress } = require("net");

const app = express()
const server = http.createServer(app);
const io = new Server(server, {
    cors: {
        origin: '*',
    }
});

const PORT_SOCKET = 5050

app.use(express.json());
app.use(cors());
app.use('/auth', routerAuth);
app.use('/', routerRooms);

io.on('connection', (socket) => {
    console.log('a user connected');
});

server.listen(PORT_SOCKET, (err) => {
    if (err) {
        throw Error('Server SOCKEET not running: ', err.message)
    }
    console.log(`Server SOCKET listening on *:${PORT_SOCKET}`);
});
