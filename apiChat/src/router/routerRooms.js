const express = require("express");

const { ROOMS } = require('../data/dataChat')
const routerRooms = express.Router()

routerRooms.get('/room', (req, res) => {
    res.status(200).json({
        status: 'success',
        code: 200,
        data: ROOMS,
    })
})

module.exports = routerRooms