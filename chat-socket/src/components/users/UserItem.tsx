import React, {ReactNode, ReactChild} from 'react'

import type { IUser } from '../types/users'

import './Users.css'

interface props {
    user: IUser
    children?: ReactChild | ReactNode
}

const UserItem: React.FunctionComponent<props> =({user}) => {
    return (
        <>
            <li className='User-item'>
                <span>{user.name} </span>
                <span>({user.age.toString()})</span>
            </li>
        </>
    )
}

export default UserItem