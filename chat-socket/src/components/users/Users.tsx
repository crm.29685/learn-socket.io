import { useState } from 'react';

import './Users.css'

import type { IUser } from '../types/users'

import UserItem from './UserItem'

function Users() {
    const [userList, setUserList] = useState<IUser[] | []>([{id: 1, name: 'UserSuper', age: 15}, {id: 2, name: 'STEPaN', age: 25}, {id: 3, name: 'Luck', age: 22}]);
    
    return (
        <div className='Users'>
            <div className="Users-count">
                Users Online: 1
            </div>
            <ul className="Users-list">
                {userList.map(user => <UserItem key={`${user.id}`} user={user}/>)}
            </ul>
        </div>
    )
}

export default Users