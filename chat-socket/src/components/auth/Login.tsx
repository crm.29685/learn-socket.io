import React, { FormEvent, useState } from 'react';

import { useAppDispatch } from '../../hooks/hooksRedux'
import operations from '../../reduxToolkit/operations/operationsUsers'

import './Auth.css'

import CustomInput from './CustomInput'

interface props {
  nameForm: string
}

const Login:React.FC<props> = ({ nameForm }) => {
  // const [errorValid, setErrorValid] = useState<boolean>(false)
  // const [errorMessage, seterrorMessage] = useState<string>('')
  const [login, setLogin] = useState<string>('')
  const [password, setPassword] = useState<string>('')
  const [secondPassword, setSecondPassword] = useState<string>('')

  const dispatch = useAppDispatch()
  const signIn = (e:FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    if (nameForm === 'SignUp') {
      if (password !== secondPassword) { 
        alert('Паролі не співпадають!!!')
        return
      }
    //   dispatch(operations.login({
    //     name: "STEPaN",
    //     password: "123"
    // }))
      console.log('SignUp user');
      // POST api SignUp
      return
    }

    console.log('Login user');
    // POST api Login
  }

  return (
  <div style={{width: "100vw", display: 'flex', alignItems: "center", justifyContent: 'center'}}>
    <form name={nameForm} className='Form' onSubmit={e => signIn(e)}>
      <h3>{nameForm}</h3>
      <CustomInput nameInput="Login" sendDataInput={setLogin}/>
      <CustomInput nameInput="Password" sendDataInput={setPassword}/>
      {
        nameForm === 'SignUp' ? 
        <CustomInput nameInput="repeat Password" sendDataInput={setSecondPassword}/> :
        null
      }
      <button className='Button' name={nameForm} type='submit' disabled={login.length === 0 || password.length === 0}>{nameForm}</button>
    </form>
  </div>
  )
}


export default Login