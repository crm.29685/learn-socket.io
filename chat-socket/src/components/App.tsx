import React, { useState } from 'react';
import './App.css';
import { ChatContext } from '../context/myContext'

import { io } from 'socket.io-client'

import Users from './users/Users'
import Chat from './chat/Chat'
import Login from './auth/Login'

import type { messageList } from './types/Chat'

const socket = io('http://localhost:5050/')

const TEST_MESSAGES = [
  {id: 11, owner: "UserSuper", message: "Hi chat!"}, 
  {id: 12, owner: "STEPaN", message: "Hi UserSuper!"},
  {id: 13, owner: "you", message: "Hi guys)"},
  {id: 14, owner: "STEPaN", message: "Hi."},
]
// export const ChatContext = createContext:IContextChat({})

function App() {
  const [messageList, setMessageList] = useState<messageList[]>(TEST_MESSAGES)

  return (
      <div className="App">
        <main className='App-main'>
          <Login nameForm="SignUp"/>
          {/* <Login nameForm="Login"/> */}
          {/* <Users />
          <ChatContext.Provider value={{ messages: messageList }}>
          <Chat />
        </ChatContext.Provider> */}
        </main>
      </div>
  );
}

export default App;
