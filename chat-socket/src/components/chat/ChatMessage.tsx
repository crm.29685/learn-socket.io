import React from 'react'

import { ChatContext, useMyContext } from '../../context/myContext'

import type { IContextChat } from '../types/Context'

const ChatMessages:React.FC = () => {
    // const { messageList, setMessageList }:IContext = useContext(ChatContext)
    const chatList:IContextChat = useMyContext(ChatContext)

    return (
        <>
        {
           chatList.messages.map(msg => {
                return (
                    <li key={msg.id.toString()} className='Chat-messages__container-item'>
                        <span className={`Chat-messages__user-name ${msg.owner === 'you' ? 'Chat-messages__user-name--owner' : ''}`}>{msg.owner}</span>

                        <span className={`Chat-messages__text ${msg.owner === 'you' ? 'Chat-messages__text--owner' : ''}`}>
                            {msg.message}
                        </span>
                    </li>
                )
            })
        }
        </>
    )
}

export default ChatMessages