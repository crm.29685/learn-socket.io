import { useState } from 'react'

import './Chat.css'

// import type { messageList } from '../types/Chat'

import ChatMessage from './ChatMessage'
import MessageInput from './ChatInput'

// const TEST_MESSAGES = [
//     {id: 11, owner: "UserSuper", message: "Hi chat!"}, 
//     {id: 12, owner: "STEPaN", message: "Hi UserSuper!"},
//     {id: 13, owner: "you", message: "Hi guys)"},
//     {id: 14, owner: "STEPaN", message: "Hi."},
// ]

function Chat() {
    const sendMessageUser = (msg:string) => {
        console.log("123", msg);
    }
    
    return (
        <div className='Chat'>
            <span className='Chat-title'>
                Wecom in chat 'UserName'
            </span>
            <div>
                <ul className='Chat-messages'>
                    <ChatMessage />
                    {/* <ChatMessage messages={messageList}/> */}
                </ul>
                <MessageInput sendMessageUser={sendMessageUser}/>
            </div>
        </div>
    )
}

export default Chat