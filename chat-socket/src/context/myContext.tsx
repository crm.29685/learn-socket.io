import React, {useContext, createContext} from 'react'
import type { IContextChat } from '../components/types/Context'

export const ChatContext = createContext<IContextChat | null>(null)

export const useMyContext = (value:React.Context<any>) => useContext(value)