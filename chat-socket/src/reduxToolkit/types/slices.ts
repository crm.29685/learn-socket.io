export interface IUser {
  id: string | null,
  name: string | null,
  online: boolean,
}

export interface IStateUser {
  user: IUser,
  isLoggedIn: boolean,
  error: null | string,
}




