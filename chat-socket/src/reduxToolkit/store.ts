import { configureStore } from "@reduxjs/toolkit";
import authReducer from './slice/sliceUsers';

const store = configureStore({
  reducer: {
    auth: authReducer,
    // chat: filtersReducer,
  },
});

export default store

export type RootState = ReturnType<typeof store.getState>;
export type AppDispath = typeof store.dispatch;