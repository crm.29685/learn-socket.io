import axios from 'axios'
import { createAsyncThunk } from '@reduxjs/toolkit';

import { IUser } from '../types/slices'
import { IUserReq } from '../types/operations'

const instanceApi = axios.create({
  baseURL: 'http://localhost:5050/auth',
});

const signIn = createAsyncThunk<IUser, IUserReq, { rejectValue: string }>(
  'auth/signIn',
  async (userData, thunkAPI) => {
    try {
      const { data } = await instanceApi.post('/signup', userData);

      return data.data;
    } catch (err: any) {
      if (err instanceof Error) {
        return thunkAPI.rejectWithValue(err.message);
      }
    }
  }
)

const login = createAsyncThunk<IUser, IUserReq, { rejectValue: string }>(
  'auth/login',
  async (userData, thunkAPI) => {
    
    try {
      const { data } = await instanceApi.post('/login', userData);
      console.log(data);
      
      return data.data;
    } catch (err: any) {
      if (axios.isAxiosError(err)) {
        console.log('error message: ', err.message);
        return err.message;
      } else {
        console.log('unexpected error: ', err);
        return 'An unexpected error occurred';
      }
    }
  }
)

const operations = {
  signIn,
  login
}

export default operations
