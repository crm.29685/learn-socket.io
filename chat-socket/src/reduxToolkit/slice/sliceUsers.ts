import { createSlice } from '@reduxjs/toolkit';

import authOperations from '../operations/operationsUsers'
import type { IStateUser } from '../types/slices'

const initialState: IStateUser = {
  user: {
    id: null,
    name: null,
    online: false,
  },
  isLoggedIn: false,
  error: null,
}

export const authSlice = createSlice({
  name: "auth",
  initialState,

  reducers: {},
  extraReducers: (builder) => {
    builder
    .addCase(authOperations.signIn.fulfilled, (state, action) => {
      state.user.id = action.payload.id;
      state.user.name = action.payload.name;
      state.user.online = action.payload.online;
      state.error = null;
    })
    .addCase(authOperations.signIn.rejected, (state, action) => {
      if (action.payload) {
        state.error = action.payload;
      }
    })
    .addCase(authOperations.login.fulfilled, (state, action) => {
      
      state.user.id = action.payload.id;
      state.user.name = action.payload.name;
      state.user.online = action.payload.online;
      state.error = null;
    })
    .addCase(authOperations.login.rejected, (state, action) => {
      if (action.payload) {
        state.error = action.payload;
      }
    });
    // login
  //   [authOperations.logIn.fulfilled](state, action:PayloadAction<IUser>) {
  //     state.user.id = action.payload.id;
  //     state.user.name = action.payload.name;
  //     state.user.online = action.payload.online;
  //     state.error = null;
  //   },
  //   [authOperations.logIn.rejected](state, action:PayloadAction<string>) {
  //     state.error = action.payload;
  //   },
  //   // Logout
  //   [authOperations.logOut.fulfilled](state) {
  //     state.user = {
  //       id: null,
  //       name: null,
  //       online: false,
  //     };

  //     state.isLoggedIn = false;
  //     state.error = null;
  //   },
  //   [authOperations.logOut.rejected](state, action) {
  //     state.error = action.payload;
  //   },
  }
})

export default authSlice.reducer;